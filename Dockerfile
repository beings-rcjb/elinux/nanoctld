ARG BASE_IMAGE=python:3
FROM ${BASE_IMAGE}

RUN pip install pipenv

RUN mkdir -p /app
WORKDIR /app
COPY Pipfile Pipfile.lock ./

RUN pipenv install --three --system --deploy

COPY . .

ENV PORT 80
EXPOSE 80

ENTRYPOINT ["python", "nanoctld.py"]
CMD []
