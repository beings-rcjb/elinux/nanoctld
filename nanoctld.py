import subprocess
from flask import Flask, request, jsonify, abort
from flask_httpauth import HTTPBasicAuth
from werkzeug.exceptions import HTTPException, default_exceptions

app = Flask(__name__)
auth = HTTPBasicAuth()


@app.errorhandler(Exception)
def handle_error(e):
    print('error')
    code = 500
    if isinstance(e, HTTPException):
        code = e.code
        einfo = e.name
    else:
        einfo = "An internal error occurred"
        print(e)  # FIXME
    return jsonify({'error': einfo}), code


admins = {
    'root': 'pass'
}


@auth.get_password
def get_pw(username):
    try:
        return admins[username]
    except KeyError:
        return None


@app.route('/power', methods=['POST'])
@auth.login_required
def power_action():
    if not request.is_json:
        abort(400)
    data = request.get_json()
    print(data)
    type = data['type']
    status = {'status': 'ok'}
    if type == 'test':
        print('test')
    if type == 'suspend':
        subprocess.call(['systemctl', 'suspend'])
    if type == 'poweroff':
        subprocess.call(['systemctl', 'poweroff'])
    if type == 'reboot':
        subprocess.call(['systemctl', 'reboot'])
    return jsonify(status), 200


@app.route('/power', methods=['GET'])
def power_query():
    return jsonify({'power': {
        'state': 'on'
    }})


for ex in default_exceptions:
    app.register_error_handler(ex, handle_error)


if __name__ == '__main__':
    from configargparse import ArgumentParser

    parser = ArgumentParser(description="Remote system control daemon")

    parser.add_argument('-p', '--port', type=int,
                        default=8080, env_var='PORT',
                        help="Server port")

    args = parser.parse_args()

    app.run(host='0.0.0.0', port=args.port)
